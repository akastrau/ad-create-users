﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace uzytkownicy_ad
{
    class ADLogon
    {
        public static PrincipalContext LoginWithCredentials(bool isAuthenticated, string domain, ref string currentIdentity)
        {
            int max_retry = 0;
            while (!isAuthenticated && max_retry < 3)
            {
                string userName = null;

                Console.WriteLine("\n\tNie mogliśmy uwierzytelnić żądania! Wprowadż dane logowania.\n");
                Console.Write("\t\tLogin: ");
                userName = Console.ReadLine();
                Console.Write("\t\tHasło: ");
                SecureString securePassword = new SecureString();
                while (true)
                {
                    ConsoleKeyInfo key = Console.ReadKey(true);
                    if (key.Key == ConsoleKey.Enter) { securePassword.MakeReadOnly(); break; }
                    else if (key.Key == ConsoleKey.Backspace)
                    {
                        if (securePassword.Length > 0)
                        {
                            securePassword.RemoveAt(securePassword.Length - 1);
                            Console.Write("\b \b");
                        }
                    }
                    else
                    {
                        securePassword.AppendChar(key.KeyChar);
                        Console.Write("*");
                    }
                }
                using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, domain, null, ContextOptions.Negotiate))
                {
                    using (SecureStringWrapper wrapper = new SecureStringWrapper(securePassword))
                    {
                        byte[] passw = new byte[securePassword.Length];
                        passw = wrapper.ToByteArray();

                        securePassword.Dispose();
                        isAuthenticated = pc.ValidateCredentials(userName, Encoding.UTF8.GetString(passw));
                        if (isAuthenticated)
                        {
                            currentIdentity = userName;
                            PrincipalContext newPC = new PrincipalContext(ContextType.Domain, domain, userName, Encoding.UTF8.GetString(passw));
                            return newPC;

                        }
                        max_retry++;
                    }

                }
            }
            return null;
        }
        public static PrincipalContext LogIn(ref string currentIdentity)
        {
            bool automaticLogonSuccessful = false;
            string domain = null;
            Console.Write("\n\t[*] Logowanie do serwera AD...");
            try
            {
                domain = IPGlobalProperties.GetIPGlobalProperties().DomainName;
            }
            catch (Exception ex)
            {
                Console.WriteLine("\n\t" + ex.Message + "\n");
                domain = null;
            }
            if (domain == null)
            {
                Console.WriteLine("\n\tKomputer nie jest częścią domeny AD!\n");
                Console.Write("\n\tNaciśnij dowolny klawisz, aby zakończyć... ");
                Console.ReadKey();
                Environment.Exit(1);
            }
            try
            {
                using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, domain, null, ContextOptions.Negotiate))
                {
                    UserPrincipal up = UserPrincipal.FindByIdentity(pc, "Administrator");
                    automaticLogonSuccessful = true;
                    PrincipalContext pc2 = new PrincipalContext(ContextType.Domain);
                    return pc2;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("\n\n\tBłąd logowania! " + ex.Message);
                PrincipalContext pc = LoginWithCredentials(automaticLogonSuccessful, domain, ref currentIdentity);
                return pc;
            }
        }
    }
}
