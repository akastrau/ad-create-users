﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uzytkownicy_ad
{
    class ProgramInfo
    {
        public string password { get; set; }
        public int length { get; set; }
        public int mode { get; set; }
        public string pattern { get; set; }
        public string pdfStringData { get; set; }
        public int modePdf { get; set; }
        public int resetPasswordOnly { get; set; }
        public int success { get; set; }
        public ProgramInfo()
        {
            password = null;
            length = 0;
            mode = -1;
            pattern = null;
            pdfStringData = null;
            modePdf = -1;
            resetPasswordOnly = 0;
            success = 0;
        }
    }
}
