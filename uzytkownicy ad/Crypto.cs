﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace uzytkownicy_ad
{
    class Crypto
    {
        public static readonly string letters = "qwertyuiopasdfghjklzxcvbnm";
        public static readonly string capitalLetters = "QWERTYUIOPASDFGHJKLZXCVBNM";
        public static readonly string numbers = "1234567890";
        public static readonly string specialCharacters = "!@#$%^&*()-=+_{}[]:;',.<>/?\"";
        public static string GenerateRandomPassword(string pattern, int length)
        {
            try
            {
                StringBuilder strBuilder = new StringBuilder();

                for (int i = 0; i < length; i++)
                {
                    using (var rnd = new RNGCryptoServiceProvider())
                    {
                        byte[] bytesForInt32 = new byte[4];
                        rnd.GetBytes(bytesForInt32);
                        int randomNumber = BitConverter.ToInt32(bytesForInt32, 0) % pattern.Length;
                        if (randomNumber < 0)
                        {
                            randomNumber += pattern.Length;
                        }
                        strBuilder.Append(pattern[randomNumber]);
                    }
                }
                return strBuilder.ToString();
            }
            catch (Exception ex)
            {
                Console.Write("\n\t" + ex.Message + "\n\tNaciśnij dowolny klawisz, aby zakończyć... ");
                Console.ReadKey();
                return null;
            }
            
        }
    }
}
