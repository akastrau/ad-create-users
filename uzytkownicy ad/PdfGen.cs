﻿using PdfSharp.Drawing;
using PdfSharp.Drawing.Layout;
using PdfSharp.Fonts;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace uzytkownicy_ad
{
    class PdfGen
    {
        public static int DecideWhat()
        {
            Console.Write("\n\tCzy generować pdf z hasłem dla każdego użytkownika? (nie - generuje jeden pdf)[T/N] ");
            var answer = new ConsoleKeyInfo();
            answer = Console.ReadKey(false);

            while (answer.Key != ConsoleKey.T && answer.Key != ConsoleKey.N)
            {
                Console.Write("\n\tCzy generować pdf z hasłem dla każdego użytkownika? (nie - generuje jeden pdf)[T/N] ");
                answer = Console.ReadKey(false);
            }
            if (answer.Key == ConsoleKey.T)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
       public static void GenerateUniquePdf(string userName, string login, string password, string currentIdentity)
        {
            string text = "Dane logowania dla " + userName + "\n" +
                "\nLogin: " + login + "\nHasło: " + password +
                "\n\nPelna nazwa logowania: " + login + "@" + System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName +
                "\n\nHasło wygenerowane " + DateTime.Now.ToString() + " przez " + currentIdentity + "@" + System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;
            PdfDocument pdf = new PdfDocument(login + ".pdf");
            pdf.Info.Title = "Hasło dla użytkownika " + login;
            pdf.Info.Creator = "AD management tool - Adrian Kastrau";
            pdf.Info.Author = currentIdentity + "@" + Environment.UserDomainName;

            PdfPage page = pdf.AddPage();
            XGraphics gfx = XGraphics.FromPdfPage(page);
            XTextFormatter textFormat = new XTextFormatter(gfx);

            XFont font = new XFont("Arial", 12);

            textFormat.DrawString(text, font, XBrushes.Black,
        new XRect(50, 50, page.Width - 50, page.Height - 50),
        XStringFormats.TopLeft);

            pdf.Close();

        }

      public static void GenerateCollectivePdf(string data, string currentIdentity)
        {
            string text = "Informacje logowania dla użytkowników domeny\n" + data + "\n\nWygenerowano " + DateTime.Now.ToString() +
                " przez " + currentIdentity + "@" + System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;
            PdfDocument pdf = new PdfDocument("Dane logowania.pdf");
            pdf.Info.Title = "Hasło dla użytkowników domeny AD";
            pdf.Info.Creator = "AD management tool - Adrian Kastrau";
            pdf.Info.Author = currentIdentity + "@" + Environment.UserDomainName;

            PdfPage page = pdf.AddPage();
            XGraphics gfx = XGraphics.FromPdfPage(page);
            XTextFormatter textFormat = new XTextFormatter(gfx);
            textFormat.Alignment = XParagraphAlignment.Left;

            XFont font = new XFont("Arial", 12);

            textFormat.DrawString(text, font, XBrushes.Black,
        new XRect(50, 50, page.Width - 50, page.Height - 50),
        XStringFormats.TopLeft);

            pdf.Close();

        }
    }
}
