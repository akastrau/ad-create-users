﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace uzytkownicy_ad
{
    class Program
    {
        static readonly string welcome = "\n\tNarzędzie do dodawania/resetowania kont domenowych w usłudze AD\n\t\tbitbucket.org/akastrau\n\t" +
                "==================================================================================";
        static readonly string version = "1.09";

        static int DecideWhat()
        {
            Console.WriteLine("\n\tGenerowanie hasła:");
            Console.Write("\n\tCzy generować unikalne hasło dla kazdego użytkownika oddzielnie?[T/N] ");
            var answer = new ConsoleKeyInfo();
            answer = Console.ReadKey(false);

            while (answer.Key != ConsoleKey.T && answer.Key != ConsoleKey.N)
            {
                Console.Write("\n\tCzy generować unikalne hasło dla kazdego użytkownika oddzielnie?[T/N] ");
                answer = Console.ReadKey(false);
            }
            if (answer.Key == ConsoleKey.T)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        static int PasswordResetPrompt()
        {
            Console.Write("\n\tCzy chcesz tylko zresetować hasła?[T/N] ");
            var answer = new ConsoleKeyInfo();
            answer = Console.ReadKey(false);

            while (answer.Key != ConsoleKey.T && answer.Key != ConsoleKey.N)
            {
                Console.Write("\n\tCzy chcesz tylko zresetować hasła?[T/N] ");
                answer = Console.ReadKey(false);
            }
            if (answer.Key == ConsoleKey.T)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }


        static void ShowHelp()
        {
            Console.WriteLine("\n\tUżycie: " + AppDomain.CurrentDomain.FriendlyName + " nazwa_pliku_z_kontami.csv OPCJE");
            Console.WriteLine("\n\tOpcje:");
            Console.WriteLine("\t\t-p HASŁO\tTworzy konta z podanym hasłem" +
                "\n\t\t-r\t\tTworzy konta z losowymi hasłami o podanej długości (małe, duże litery, cyfry)" +
                "\n\t\t-r WZORZEC\tTworzy konta z losowymi hasłami o podanej długości (wedlug gotowych wzorców)" +
                "\n\t\t-ro WZORZEC\tTworzy konta z losowymi hasłami o podanej długości (użytkownik wprowadza wzorzec)" +
                "\n\t\t-v\t\tPokazuje wersje programu" +
                "\n\n\tGOTOWE WZORCE:" +
                "\n\tl\t\tlitery małe" +
                "\n\tCL\t\tlitery duże" +
                "\n\ts\t\tznaki specjalne" +
                "\n\tn\t\tcyfry" +
                "\n\n\tPrzykłady użycia:" +
                "\n\t\t" + AppDomain.CurrentDomain.FriendlyName + " ad.csv -p SuperTajneHasloAD2016" +
                "\n\t\t" + AppDomain.CurrentDomain.FriendlyName + " ad.csv -r CLns\tWygeneruje haslo z dużymi literami, znakami specjalnymi i cyframi" +
                "\n\t\t" + AppDomain.CurrentDomain.FriendlyName + " -v\tPokaże numer wersji programu" +
                "\n\n\tUWAGA! Program generuje hasła o długości nie większej niż 25 znaków!" +
                "\n\tMożliwe jest tylko resetowania haseł (bez tworzenia kont)");

            Console.Write("\n\tNaciśnij dowolny klawisz, aby zakończyć... ");
            Console.ReadKey();
            Environment.Exit(1);
        }

        static void Main(string[] args)
        {
            string password = null;
            int length = 0;
            int mode = -1;
            string pattern = null;
            string pdfStringData = null;
            int modePdf = -1;
            int resetPasswordOnly = -1;
            int success = 0;
            string userName = Environment.UserName;
            Console.Title = "Dodaj konta Active Directory";
            Console.WriteLine(welcome);
            if (args.Length < 1)
            {
                ShowHelp();
            }

            if (args[0] == "-v")
            {
                Console.WriteLine("\n\tWersja: " + version);
                Environment.Exit(0);
            }

            else if (args.Length == 3 && args[1] == "-p" && args[2].Trim() != null)
            {
                password = args[2].Trim();
                modePdf = PdfGen.DecideWhat();
                resetPasswordOnly = PasswordResetPrompt();
            }
            else if (args.Length == 2 && args[1] == "-r")
            {
                pattern = Crypto.letters + Crypto.capitalLetters + Crypto.numbers;
                mode = DecideWhat();

                Console.Write("\n\tLiczba znaków w haśle: ");
                while (!int.TryParse(Console.ReadLine(), out length)  || (length <= 0 || length > 25))
                {
                    Console.Write("\n\tLiczba znaków w haśle: ");
                }
                Console.WriteLine();
                modePdf = PdfGen.DecideWhat();
                resetPasswordOnly = PasswordResetPrompt();

            }
            else if (args.Length == 3 && args[1] == "-r" && args[2].Trim() != null)
            {
                if (args[2].Trim().Contains("CL")) pattern += Crypto.capitalLetters;
                if (args[2].Trim().Contains('l')) pattern += Crypto.letters;
                if (args[2].Trim().Contains('n')) pattern += Crypto.numbers;
                if (args[2].Trim().Contains('s')) pattern += Crypto.specialCharacters;

                if (pattern == null)
                {
                    ShowHelp();
                }
                
                mode = DecideWhat();

                Console.Write("\n\tLiczba znaków w haśle: ");
                while (!int.TryParse(Console.ReadLine(), out length) || (length <= 0 || length > 25))
                {
                    Console.Write("\n\tLiczba znaków w haśle: ");
                }
                Console.WriteLine();
                modePdf = PdfGen.DecideWhat();
                resetPasswordOnly = PasswordResetPrompt();
            }
            else if (args.Length == 3 && args[1] == "-ro" && args[2].Trim() != null)
            {
                pattern = args[2].Trim();
                mode = DecideWhat();

                Console.Write("\n\tLiczba znaków w haśle: ");
                while (!int.TryParse(Console.ReadLine(), out length) || (length <= 0 || length > 25))
                {
                    Console.Write("\n\tLiczba znaków w haśle: ");
                }
                Console.WriteLine();
                modePdf = PdfGen.DecideWhat();
                resetPasswordOnly = PasswordResetPrompt();
            }
            else
            {
                ShowHelp();
            }

            try
            {
                //Czytaj plik
                Console.WriteLine("\n\t[*] Otwieram plik...");
                var reader = new StreamReader(args[0], Encoding.Default);
                //Przygotuj liste - nazwa wyświetlana, login
                List<string> Name = new List<string>();
                List<string> samAccountName = new List<string>();
                List<string> passwords = new List<string>();

                Console.WriteLine("\t[*] Tworzę liste użytkowników...");

                while (!reader.EndOfStream)
                {
                    //Wczytaj pierwsza komorke jako nazwe uzytkownika
                    var name = reader.ReadLine().Split(',')[0];
                    Name.Add(name);
                    //Podziel nazwe uzytkownika na imie i nazwisko
                    var samAccount = name.Split(' ');
                    //Jezeli nazwa uzytkownika zawiera imie - wyswietl imie
                    if (samAccount.Length < 2)
                    {
                        //Zamiana polskich znakow na zwykle
                        byte[] tempBytes;
                        tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(samAccount[0]);
                        string asciiStr = System.Text.Encoding.UTF8.GetString(tempBytes);
                        var properName = asciiStr;
                        samAccountName.Add(properName);
                        //Console.WriteLine("\n\tUżytkownik: " + name + " Nazwa konta: " + properName + "\n");
                    }
                    else
                    {
                        byte[] tempBytes;
                        tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(samAccount[1]);
                        string asciiStr = System.Text.Encoding.UTF8.GetString(tempBytes);
                        var properName = asciiStr + '-';
                        tempBytes = System.Text.Encoding.GetEncoding("ISO-8859-8").GetBytes(samAccount[0]);
                        asciiStr = System.Text.Encoding.UTF8.GetString(tempBytes);
                        properName += asciiStr[0];
                        samAccountName.Add(properName);

                        //Console.WriteLine("\n\tUżytkownik: " + name + " Nazwa konta: " + properName + "\n");
                    }
                }
                reader.Close();
                reader.Dispose();

                if (mode == 1)
                {
                    Console.WriteLine("\t[*] Tworzę liste haseł...");
                    for (int i = 0; i < Name.Count; i++)
                    {
                        passwords.Add(Crypto.GenerateRandomPassword(pattern, length));
                    }
                }

               else if (mode == 0)
                {
                    password = Crypto.GenerateRandomPassword(pattern, length);
                }
                string nameString = "Nazwa";
                string loginString = "Login";
                string passwordString = "Hasło";
                Console.WriteLine(string.Format("\t{0,30}{1,30}{2,31}", nameString, loginString, passwordString) +
                    "\n\t====================================================================================================");
                pdfStringData += string.Format("\t{0,30}{1,30}{2,31}", nameString, loginString, passwordString) +
                    "\t======================================================\n";
                for (int i = 0; i != Name.Count; i++)
                {
                    if (mode == 1)
                    {
                        Console.WriteLine(string.Format("\t{0, 30}{1,30}{2,31}", Name[i], samAccountName[i], passwords[i]));
                    }
                    else
                    {
                        Console.WriteLine(string.Format("\t{0, 30}{1,30}{2,31}", Name[i], samAccountName[i], password));
                    }
                }
                var answer = new ConsoleKeyInfo();
                if (resetPasswordOnly == 0)
                {
                    Console.Write("\n\tCzy dodać te konta? [T/N]: ");
                    answer = Console.ReadKey(false);

                    while (answer.Key != ConsoleKey.T && answer.Key != ConsoleKey.N)
                    {
                        Console.Write("\n\tCzy dodać te konta? [T/N]: ");
                        answer = Console.ReadKey(false);
                    }
                }
                else
                {
                    Console.Write("\n\tCzy chcesz zresetować hasła dla tych kont? [T/N]: ");
                    answer = Console.ReadKey(false);

                    while (answer.Key != ConsoleKey.T && answer.Key != ConsoleKey.N)
                    {
                        Console.Write("\n\tCzy chcesz zresetować hasła dla tych kont? [T/N]: ");
                        answer = Console.ReadKey(false);
                    }
                }

                if (answer.Key == ConsoleKey.T)
                {
                    long failed = 0;

                    using (var pc = ADLogon.LogIn(ref userName))
                    {
                        if (pc == null)
                        {
                            Console.WriteLine("\n\tUżytkownik nie zalogował się poprawnie na konto domenowe!\n");
                            Console.Write("\n\tNaciśnij dowolny klawisz, aby zakończyć... ");
                            Console.ReadKey();
                            Environment.Exit(1);
                        }


                        Console.WriteLine();
                        if (resetPasswordOnly == 1)
                        {
                            for (int i = 0; i < samAccountName.Count; i++)
                            {
                                Console.Write("\t[*] Resetowanie hasła " + (i + 1) + " z " + samAccountName.Count + "\r");
                                UserPrincipal userPrincipalCheck = UserPrincipal.FindByIdentity(pc, samAccountName[i]);
                                if (userPrincipalCheck != null)
                                {
                                    if (mode == 1)
                                    {
                                        userPrincipalCheck.SetPassword(passwords[i]);

                                        if (modePdf == 1)
                                        {
                                            PdfGen.GenerateUniquePdf(Name[i], samAccountName[i], passwords[i], userName);
                                        }
                                        else if (modePdf == 0)
                                        {
                                            pdfStringData += string.Format("{0, 30}{1,30}{2,31}", Name[i], samAccountName[i], passwords[i]) + "\n";
                                        }
                                    }
                                    else
                                    {
                                        userPrincipalCheck.SetPassword(password);

                                        if (modePdf == 1)
                                        {
                                            PdfGen.GenerateUniquePdf(Name[i], samAccountName[i], password, userName);
                                        }
                                        else if (modePdf == 0)
                                        {
                                            pdfStringData += string.Format("{0, 30}{1,30}{2,31}", Name[i], samAccountName[i], password) + "\n";
                                        }
                                    }
                                    userPrincipalCheck.ExpirePasswordNow();
                                    userPrincipalCheck.Save();
                                    userPrincipalCheck.Dispose();
                                    success++;
                                }

                            }
                            Console.WriteLine("\n\n\tZresetowano haseł " + (success));
                            if (success - samAccountName.Count != 0)
                            {
                                Console.WriteLine("\n\n\tNie udało się zresetować hasła dla niektórych użytkowników\n");
                            }
                        }
                        else
                        {
                            for (int i = 0; i < Name.Count; i++)
                            {
                                Console.Write("\t[*] Dodawanie użytkowników " + (i + 1) + " z " + Name.Count + "\r");
                                UserPrincipal userPrincipalCheck = UserPrincipal.FindByIdentity(pc, samAccountName[i]);
                                if (userPrincipalCheck != null)
                                {
                                    failed++;
                                    userPrincipalCheck.Dispose();
                                    continue;
                                }
                                using (var up = new UserPrincipal(pc))
                                {
                                    up.UserPrincipalName = samAccountName[i];
                                    up.Name = Name[i];
                                    up.DisplayName = Name[i];
                                    up.GivenName = Name[i].Split(' ')[0];
                                    if (Name[i].Split(' ').Length == 2)
                                    {
                                        up.Surname = Name[i].Split(' ')[1];
                                    }
                                    up.SamAccountName = samAccountName[i];
                                    if (mode == 1)
                                    {
                                        up.SetPassword(passwords[i]);

                                        if (modePdf == 1)
                                        {
                                            PdfGen.GenerateUniquePdf(Name[i], samAccountName[i], passwords[i], userName);
                                        }
                                        else if (modePdf == 0)
                                        {
                                            pdfStringData += string.Format("{0, 30}{1,30}{2,31}", Name[i], samAccountName[i], passwords[i]) + "\n";
                                        }
                                    }
                                    else
                                    {
                                        up.SetPassword(password);

                                        if (modePdf == 1)
                                        {
                                            PdfGen.GenerateUniquePdf(Name[i], samAccountName[i], password, userName);
                                        }
                                        else if (modePdf == 0)
                                        {
                                            pdfStringData += string.Format("{0, 30}{1,30}{2,31}", Name[i], samAccountName[i], password) + "\n";
                                        }
                                    }
                                    up.Enabled = true;
                                    up.ExpirePasswordNow();
                                    up.Save();
                                    up.Dispose();
                                }
                            }
                            Console.WriteLine("\n\n\tDodanych użytkowników: " + (Name.Count - failed));
                            if (failed != 0)
                            {
                                Console.WriteLine("\n\n\tNiektórzy użytkownicy nie zostali dodani, ponieważ już istnieją w domenie AD\n");
                            }
                        }

                    }
                    GC.Collect();
                    GC.WaitForFullGCApproach();
                        
                    if (modePdf == 0)
                    {
                        Console.Write("\t[*] Generowanie pdfa... ");
                        PdfGen.GenerateCollectivePdf(pdfStringData, userName);
                        //using (StreamWriter writer = new StreamWriter("Dane logowania.txt"))
                        //{
                        //    writer.WriteLine(pdfStringData);
                        //}
                        System.Diagnostics.Process.Start("Dane logowania.pdf");
                        Console.WriteLine();
                    }
                   
                } 
                
            }
            catch (Exception ex)
            {
                Console.Write("\n\t"+ ex.Message + "\n\tNaciśnij dowolny klawisz, aby zakończyć... ");
                Console.ReadKey();
                Environment.Exit(1);
            }
            Console.Write("\n\tNaciśnij dowolny klawisz, aby zakończyć... ");
            Console.ReadKey();
        }
    }
}
